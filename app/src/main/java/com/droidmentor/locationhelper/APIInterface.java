package com.droidmentor.locationhelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by ITD-STU on 21/07/2019.
 */

public interface APIInterface {
    @FormUrlEncoded
    @POST("UserLogin2.php")
    Call<ResponseBody> loginRequest(@Field("email") String email,
                                    @Field("password") String password);

    // Fungsi ini untuk memanggil API REGISTRASI
    @FormUrlEncoded
    @POST("UserRegistration.php")
    Call<ResponseBody> registerRequest(@Field("nama") String nama,
                                       @Field("email") String email,
                                       @Field("password") String password);
    @Headers("Content-Type: application/json")
    @FormUrlEncoded
    @POST("checkOut.php")
    Call<Value> insertLocation(@Field("location") String location,
                               @Field("tanggal_waktu") String tanggal_waktu);

    @FormUrlEncoded
    @POST("http://192.168.43.232/absensi-php-backend-master/addCheckout.php")
    Call<Value> checkOut(@Field("id_user") String id_user,
                         @Field("tanggal_waktu") String tanggal_waktu,
                         @Field("status") String status);

}
