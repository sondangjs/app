package com.droidmentor.locationhelper;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by ITD-STU on 23/07/2019.
 */

public interface CheckOutAPI {
    @Headers("Content-Type: application/json")
    @FormUrlEncoded
    @POST("http://192.168.43.232/absensi-php-backend-master/checkOut.php")
    Call<Value> insertLocation(@Field("location") String location,
                               @Field("tanggal_waktu") String tanggal_waktu);
    @FormUrlEncoded
    @POST("http://192.168.43.232/absensi-php-backend-master/addCheckout.php")
    Call<Value> checkOut(@Field("id_user") String id_user,
                         @Field("tanggal_waktu") String tanggal_waktu,
                         @Field("status") String status);

}
