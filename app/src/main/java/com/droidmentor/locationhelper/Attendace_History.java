package com.droidmentor.locationhelper;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Attendace_History extends AppCompatActivity {
    TextView tanggal1, lokasi, tanggal2;
    public static String a, b,c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendace__history);

        //gatau ini apa...
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        tanggal1 = (TextView) findViewById(R.id.tanggal);
        tanggal2 = (TextView) findViewById(R.id.endTangggal);
        lokasi = (TextView) findViewById(R.id.location);

        InputStream inputStream = null;
        String line = null, result = null, data[];
        String time = getToday();

        try {
            URL url = new URL("http://192.168.43.232/absensi-php-backend-master/getData.php?waktu="+time);
            HttpURLConnection httpsURLConnection = (HttpURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("GET");

            inputStream = new BufferedInputStream(httpsURLConnection.getInputStream());

        } catch (MalformedURLException e) {
            Log.e("error na : ",e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // read input stream into a string
        try{
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }

            inputStream.close();
            result = stringBuilder.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Parse json data
        try{

            JSONObject jsonObject = new JSONObject(result);
            JSONArray array = jsonObject.getJSONArray("user");

            for (int i=0; i<array.length(); i++){
                JSONObject d = array.getJSONObject(i);
                a = d.getString("location");
                b = d.getString("tanggalWaktu");
                c = d.getString("tanggal_waktu");
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        lokasi.setText(a);
        tanggal1.setText(b);
        tanggal2.setText(c);
    }
    public String getToday(){
        DateFormat date = new SimpleDateFormat("EEEE,yyyy/M/d");
        Date dates = new Date();
        return date.format(dates);
    }
}

