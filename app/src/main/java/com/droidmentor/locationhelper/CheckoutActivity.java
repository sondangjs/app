package com.droidmentor.locationhelper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.droidmentor.locationhelper.LocationUtil.SessionManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckoutActivity extends AppCompatActivity {
    Button checkout;
    public static final String URL = "http://192.168.43.232/absensi-php-backend-master/addCheckout.php";
    private ProgressDialog progress;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        sessionManager = new SessionManager(this);

        HashMap<String,String>user = sessionManager.getUserDetails();
        final String status = user.get(SessionManager.status);
        final String tanggal = user.get(SessionManager.tanggal);
        final String bataswaktu = Date();
        int tahun = Integer.parseInt(bataswaktu.substring(0,4));
        int bulan = Integer.parseInt(bataswaktu.substring(5,6));
        int batas_tanggal = Integer.parseInt(bataswaktu.substring(7,9));
        int totalhari = ((tahun*366) + (bulan*31) + batas_tanggal);

        checkout = (Button) findViewById(R.id.checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress = new ProgressDialog(CheckoutActivity.this);
                progress.setCancelable(false);
                progress.setMessage("Loading ...");
                progress.show();

                final String id_user ="1";
                final String status = "Check Out";

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(URL).addConverterFactory(GsonConverterFactory.create())
                        .build();

                //CONNECT KE API CHECKOUTNYA
                APIInterface pesanAPI = retrofit.create(APIInterface.class);
                Call<Value> call = pesanAPI.checkOut(id_user, getToday(), status);
                call.enqueue(new Callback<Value>() {
                    @Override
                    public void onResponse(Call<Value> call, Response<Value> response) {
                        String value = response.body().getValue();
                        String massage = response.body().getMessage();
                        progress.dismiss();
                        if(value.equals("1")){
                            Toast.makeText(CheckoutActivity.this, massage, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(CheckoutActivity.this, MainActivity.class));
                        }
                        else{
                            Toast.makeText(CheckoutActivity.this, massage, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Value> call, Throwable t) {
                        progress.dismiss();
                        Log.e("message : ",t.getMessage());
                        Toast.makeText(CheckoutActivity.this, "Jaringan Error", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }
    public String getToday(){
        Date current = new Date();
        SimpleDateFormat frmt = new SimpleDateFormat("EEEE, dd-MM-yyyy HH:mm:ss");
        String dateString = frmt.format(current);
        return dateString;
    }
    public String Date(){
        Date current = new Date();
        SimpleDateFormat frmt = new SimpleDateFormat("dd-MM-yyyy ");
        String dateString = frmt.format(current);
        return dateString;
    }

    }
