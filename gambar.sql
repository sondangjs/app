/*
SQLyog Community v12.5.1 (64 bit)
MySQL - 10.1.30-MariaDB : Database - gambar
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gambar` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `gambar`;

/*Table structure for table `checkout` */

DROP TABLE IF EXISTS `checkout`;

CREATE TABLE `checkout` (
  `id` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` bigint(100) NOT NULL,
  `tanggal_waktu` varchar(1000) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `checkout` */

insert  into `checkout`(`id`,`id_user`,`tanggal_waktu`,`status`) values 
(1,1,'test','checkout'),
(2,1,'','Check Out'),
(3,1,'','Check Out'),
(4,1,'','Check Out'),
(5,1,'','Check Out'),
(6,1,'','Check Out'),
(7,1,'','Check Out'),
(8,1,'','Check Out'),
(9,1,'','Check Out'),
(10,1,'','Check Out');

/*Table structure for table `gambar` */

DROP TABLE IF EXISTS `gambar`;

CREATE TABLE `gambar` (
  `id_gambar` bigint(100) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(1000) NOT NULL,
  PRIMARY KEY (`id_gambar`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `gambar` */

insert  into `gambar`(`id_gambar`,`gambar`) values 
(1,'C:/xampp/htdocs/UploadWebService/tempatGambarnya/0.png'),
(2,'C:/xampp/htdocs/UploadWebService/tempatGambarnya/1.png'),
(3,'C:/xampp/htdocs/UploadWebService/tempatGambarnya/2.png'),
(4,'C:/xampp/htdocs/UploadWebService/tempatGambarnya/3.png');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`email`,`password`) values 
(1,'Gabriel','if417020','bangjait23');

/*Table structure for table `userlogin` */

DROP TABLE IF EXISTS `userlogin`;

CREATE TABLE `userlogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(1000) NOT NULL,
  `tanggalWaktu` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userlogin` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
