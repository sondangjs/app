package com.droidmentor.locationhelper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by ITD-STU on 26/07/2019.
 */

public class Home extends AppCompatActivity {
    TextView history, absensi;
//   Layout histori, foto;
    Context mContext;
    TextView tvResultNama;
    SharedPrefManager sharedPrefManager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);

        sharedPrefManager = new SharedPrefManager(this);
     //   tvResultNama.setText(sharedPrefManager.getSPNama());

        history = (TextView) findViewById(R.id.history);
        absensi = (TextView) findViewById(R.id.absensi);

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this,Attendace_History.class));
            }
        });

        absensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this,Dashboard.class));
            }
        });
    }
}