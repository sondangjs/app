package com.droidmentor.locationhelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;

public class Dashboard extends AppCompatActivity {

    Button takeSelfieBtn,sendLocationtbtn;
//    TextView tvResultNama;
//    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

//        sharedPrefManager = new SharedPrefManager(this);
//        tvResultNama.setText(sharedPrefManager.getSPNama());

        takeSelfieBtn = (Button) findViewById(R.id.takeSelfieBtn);
        sendLocationtbtn = (Button) findViewById(R.id.sendLocationtbtn);

        takeSelfieBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this,SelfieCamera.class));
            }
        });

        sendLocationtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this,MyLocationUsingLocationAPI.class));
            }
        });

    }
}
