package com.droidmentor.locationhelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.droidmentor.locationhelper.LocationUtil.LocationHelper;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

public class SelfieCamera extends AppCompatActivity {
    public static final String UPLOAD_KEY = "image";
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int CAMERA_REQUEST_CODE = 7777;
    private Button take;
    private ImageView image,send;
    private Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfie_camera);
        take = (Button) findViewById(R.id.take);
        image = (ImageView) findViewById(R.id.gambar);
        send = (ImageView) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
                Toast.makeText(SelfieCamera.this,"Berhasil upload foto",Toast.LENGTH_SHORT).show();
            }
        });

        take.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);

                    //disable button
                    take.setEnabled(false);
                }
            }
        });

    }
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case(CAMERA_REQUEST_CODE):
                if(resultCode == Activity.RESULT_OK)
                {
                    bitmap = (Bitmap) data.getExtras().get("data");
                    image.setImageBitmap(bitmap);
                }
                break;
        }
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos =new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return encodedImage;
    }
    private void uploadImage() {
        class UploadImage extends AsyncTask<Bitmap,Void,String>{
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();


            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(SelfieCamera.this,"uploading..","please wait",true,true);
            }
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(),s, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getApplication(),"this is kuteng",Toast.LENGTH_LONG).show();
            }


            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String uploadImage = getStringImage(bitmap);
                HashMap<String,String> data = new HashMap<String, String>();
                data.put(UPLOAD_KEY,uploadImage);
                String result = rh.sendPostRequest("http://192.168.43.232/UploadWebService/UploadGambar.php",data);
                return result;
            }
        }
        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
//        Toast("Couldn't get the location. Make sure location is enabled on the device");
    }
}
