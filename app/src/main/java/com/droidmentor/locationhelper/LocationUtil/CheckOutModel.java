package com.droidmentor.locationhelper.LocationUtil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ITD-STU on 25/07/2019.
 */

public class CheckOutModel {
    @SerializedName("id_user")
    @Expose
    private String is_user;
    @SerializedName("tanggal_waktu")
    @Expose
    private String tanggal_waktu;
    @SerializedName("status")
    @Expose
    private String status;

    public CheckOutModel(String status, String is_user, String tanggal_waktu){
        this.status = status;
        this.is_user = is_user;
        this.tanggal_waktu = tanggal_waktu;
    }

    public String getIs_user() {
        return is_user;
    }

    public void setIs_user(String is_user) {
        this.is_user = is_user;
    }

    public String getTanggal_waktu() {
        return tanggal_waktu;
    }

    public void setTanggal_waktu(String tanggal_waktu) {
        this.tanggal_waktu = tanggal_waktu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
