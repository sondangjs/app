package com.droidmentor.locationhelper.LocationUtil;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.droidmentor.locationhelper.Dashboard;
import com.droidmentor.locationhelper.Home;
import com.droidmentor.locationhelper.MainActivity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import okhttp3.OkHttpClient;

/**
 * Created by ITD-STU on 26/07/2019.
 */

public class BackgroundWorker extends AsyncTask<String,Void,String>{
    private OkHttpClient client = new OkHttpClient();
    Context context;
    AlertDialog alert;
    SessionManager session;
    public static String email;
    public static String id, name;
    public static String pass, data;

    public BackgroundWorker(Context ctext){
        context = ctext;
    }
    protected String doInBackground(String... params) {
        session = new SessionManager(context);
        String type = params[0];
        String username = params[1];
        String password = params[2];
        email = username;
        pass = password;
        String url_login = "http://192.168.43.232/absensi-php-backend-master/UserLogin2.php";
        if(type.equals("login")){
            try{
                URL url = new URL(url_login);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream out = httpURLConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                String postData = URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(username, "UTF-8")+"&"
                        +URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(password, "UTF-8");
                bw.write(postData);
                bw.flush();
                bw.close();
                out.close();
                InputStream input = httpURLConnection.getInputStream();
                StringBuilder sb = new StringBuilder();
                BufferedReader bf = new BufferedReader(new InputStreamReader(input, "iso-8859-1"));
                String result="";
                String line="";
                while((line = bf.readLine())!=null){
                    sb.append(line).append("\n");
                    result+= line;
                    if(result.equals("Login Success")){
                      //  session.createSession(getEmail());
                        context.startActivity(new Intent(context, Home.class));
                    }
                }
                bf.close();
                input.close();
                httpURLConnection.disconnect();
                return sb.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        alert = new AlertDialog.Builder(context).create();
        alert.setTitle("Login Status");

    }

    @Override
    protected void onPostExecute(String sb) {
        alert.setMessage(sb);
        alert.show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    public String getEmail(){
        return email;
    }
    public String getPass(){
        return pass;
    }
    public String getName(){
        return name;
    }
    public String getID(){
        return id;
    }
}


