package com.droidmentor.locationhelper;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.droidmentor.locationhelper.LocationUtil.SessionManager;

public class SplashScreen extends AppCompatActivity {
SessionManager sessionManager;
ProgressDialog pg;

    Animation fromtop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);



        sessionManager = new SessionManager(getApplicationContext());
        Handler handler = new Handler();
        pg = new ProgressDialog(this);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pg.setTitle("Installing Data.....");
                sessionManager.checkLogin();
                finish();
            }
        }, 5000);

    }
}
