package com.droidmentor.locationhelper.LocationUtil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.droidmentor.locationhelper.Home;
import com.droidmentor.locationhelper.LoginActivity;
import com.droidmentor.locationhelper.MainActivity;

import java.util.HashMap;

/**
 * Created by ITD-STU on 22/07/2019.
 */

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int mode;

    private static final String pref_name = "sondang";
    private static final String is_login = "isLogin";
    public static final String email  = "email";
    public static final String nama ="sondang";
    public static final String password = "password";
    public static final String id_user = "id_user";
    public static final String status = "status";
    public static final String tanggal = "tanggal";

    public SessionManager(Context context){
        this.context = context;
        pref = context.getSharedPreferences(pref_name, mode);
        editor = pref.edit();
    }

    public void createSession(String mail){
        editor.putBoolean(is_login, true);
        editor.putString(email, mail);
        editor.apply();
        editor.commit();
    }
    public void createStatus(String kondisi){
        editor.putBoolean(is_login, true);
        editor.putString(status, kondisi);
        editor.apply();
        editor.commit();
    }
    public void createTanggal(String date){
        editor.putBoolean(is_login, true);
        editor.putString(tanggal, date);
        editor.apply();
        editor.commit();
    }

    public void checkLogin(){
        if (!this.is_login()){
            Intent i = new Intent(context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }else {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    private boolean is_login() {
        return pref.getBoolean(is_login, false);
    }

    public void logout(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(pref_name, pref.getString(pref_name, null));
        user.put(email, pref.getString(email, null));
        user.put(password, pref.getString(password, null));
        user.put(nama, pref.getString(nama, null));
        user.put(id_user, pref.getString(id_user, null));
        user.put(tanggal, pref.getString(tanggal, null));
        user.put(status, pref.getString(status, null));
        return user;
    }
}
