package com.droidmentor.locationhelper;

/**
 * Created by ITD-STU on 21/07/2019.
 */

public class UtilsApi {
    public static final String BASE_URL_API =  "http://192.168.43.232/absensi-php-backend-master/";
    public static APIInterface getAPIService(){
        return APIClient.getClient(BASE_URL_API).create(APIInterface.class);
    }
}
