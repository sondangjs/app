package com.droidmentor.locationhelper;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ITD-STU on 21/07/2019.
 */

public class APIClient {
    private static Retrofit retrofit = null;

    private static final String BASE_URL = "http://yukmarry.com/dishub/baru/";

    private static final String base_url2 = "http://yukmarry.com/absensi/";

    private static final String URL_DEV = "https://188.166.244.230/dpm_backup/public/API/";

    private static final String URL_DEV_BPSDM = "http://202.61.105.99/admin/public/API/";

    public static Retrofit getClient(String baseUrl) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}